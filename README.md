Requisitos
- Python
- Postgres

Instruções de Instalação

- Baixar o repositório
- Mudar ou não o nome da pasta
- abrir cmd na pasta
- Criar a venv com py -m venv venv(path||padrão do path é venv)
- pip install -r "requirements.txt"

- mudar as configurações da database em settings.py

!!Avisos!!
- py manage.py só funciona na pasta onde está manage.py (3 pessoas se confundiram por causa disso incluindo eu)
