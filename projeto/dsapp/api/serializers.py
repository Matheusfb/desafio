from rest_framework import serializers
from dsapp.models import Comment, Message, UserInfo

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id', 'body', 'added_by', 'created_date', 'message']

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'body', 'created_date', 'added_by']

class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfo
        fields = ['id', 'username', 'email', 'add', 'cep', 'cpf', 'user_id']
