from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from django.http import HttpResponse

from .serialzers import UserSerializer


class CreateUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserSerializer

class OiView(CreateAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    def oi(request):
        html = "<html><body>Opa!</body></html>"
        return HttpResponse(html)

