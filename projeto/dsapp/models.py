from django.db import models
from django.conf import settings
from django.utils import timezone
# comment
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
import uuid


class Message(models.Model):
  body = models.CharField(max_length=300, default=None,)
  added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1,)
  created_date = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.body

class Comment(models.Model):
  body = models.CharField(max_length=300, default=None,)
  added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=None,)
  created_date = models.DateTimeField(default=timezone.now)
  message = models.ForeignKey(Message, on_delete=models.CASCADE, default=None,)

  def __str__(self):
    return self.body

class UserInfo(models.Model):
  username = models.CharField(max_length=200, default=None,)
  email = models.EmailField(max_length=300, default=None,)
  add = models.CharField(max_length=255, default=None,)
  user_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1,)
  cep = models.BigIntegerField(
        default=10000000,
        validators=[
            MaxValueValidator(99999999),
            MinValueValidator(10000000)
        ]
     )
  cpf = models.BigIntegerField(
        default=10000000,
        validators=[
            MaxValueValidator(99999999999),
            MinValueValidator(10000000000)
        ]
     )
  created_date = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.username
