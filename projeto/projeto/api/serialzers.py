from rest_framework import serializers
from django.contrib.auth import get_user_model # If used custom user model
import re

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    #cep = serializers.IntegerField(max_value=99999999, min_value=10000000, label="CEP")
    #cpf = serializers.IntegerField(max_value=99999999999, min_value=10000000000, label="CPF")
    #address = serializers.CharField(max_length=255, label="Endereço")

    password = serializers.CharField(style={'input_type': 'password'}, write_only=True, label="Senha",
                                    max_length=50, min_length=6,
                                    )
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True, label="Confirmar Senha",
                                    max_length=50, min_length=6,
                                    )

    email = serializers.EmailField(max_length=100, min_length=None, allow_blank=False, required=True)

    def post(self, request, format=None):
        print(request.user.id)
        user = request.user.id
        added_by = user
        serializer = MessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, validated_data):
        user = UserModel.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )
        return user

    class Meta:
        model = UserModel
        fields = ( "id", "username", "password", "password2", "email")
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        account = UserModel(
            email=self.validated_data['email'],
            username=self.validated_data['username'],
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if re.search('[A-Z]', password) is None:
            raise serializers.ValidationError({'password': 'senha não contem letra maiúscula'})
        if re.search('[A-Z]', password2) is None:
            raise serializers.ValidationError({'password2': 'senha não contem letra maiúscula'})
        if password != password2:
            raise serializers.ValidationError({'password': 'as senhas não são iguais'})
        account.set_password(password)
        account.save()
        return account

