"""projeto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static

from .api.viewsets import CreateUserView, OiView
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

from dsapp import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('messages/', views.get_messages),
    path('messages/add/', views.CreateMessageView.as_view()),
    path('messages/<int:message_id>/comment/', views.CreateCommentView.as_view()),
    path('messages/<int:message_id>/comments/', views.get_comments),
    path('messages/<int:message_id>/update/', views.UpdateMessageView.as_view()),
    path('messages/<int:message_id>/delete/', views.delete_message),
    path('info/', views.get_info),
    re_path(r'^users/', views.get_users),
    re_path(r'^login/', obtain_jwt_token),
    re_path(r'^api-token-refresh/', refresh_jwt_token),
    re_path(r'^api-token-verify/', verify_jwt_token),
    re_path(r'^register/', CreateUserView.as_view()),
    re_path(r'^re-register/', views.CreateInfoView.as_view()),
    re_path(r'^alert/', OiView.as_view()),
]
from rest_framework_jwt.views import verify_jwt_token

# ...
