from django.contrib import admin
from dsapp.models import Comment, Message, UserInfo

admin.site.register(UserInfo)
admin.site.register(Message)
admin.site.register(Comment)
