from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404 as goo

from .api.serializers import MessageSerializer, InfoSerializer, CommentSerializer
from projeto.api.serialzers import UserSerializer
from .models import Message, Comment, UserInfo
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
import json


@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_users(request):
    users = get_user_model().objects.all()   # organizadas #
    serializer = UserSerializer(users, many=True)
    return JsonResponse({'users': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_messages(request):
    user = request.user.id
    messages = Message.objects.order_by('created_date')   # organizadas #
    serializer = MessageSerializer(messages, many=True)
    return JsonResponse({'messages': serializer.data}, safe=False, status=status.HTTP_200_OK)


class CreateMessageView(CreateAPIView):
    model = Message
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = MessageSerializer

    def post(self, request, format=None):
        payload = json.loads(json.dumps(request.data))
        user = request.user
        try:
            message = Message.objects.create(
                body=payload["body"],
                added_by=user,
            )
            serializer = MessageSerializer(message) #data=request.data
            return JsonResponse({'message': serializer.data}, safe=False, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return JsonResponse({'error': 'Something terrible went wrong'}, safe=False,
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# ///   Comentários  /// #
class CreateCommentView(CreateAPIView):
    model = Comment
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = CommentSerializer

    def post(self, request, message_id, format=None):
        payload = json.loads(json.dumps(request.data))
        user = request.user
        message = Message.objects.get(id=message_id)
        try:
            comment = Comment.objects.create(
                body=payload["body"],
                added_by=user,
                message=message,
            )
            serializer = CommentSerializer(comment)
            return JsonResponse({'comment': serializer.data}, safe=False, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
        #except Exception:
        #    return JsonResponse({'error': 'Something terrible went wrong'}, safe=False,
        #                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """def post(self, request, message_id, format=None):
        print(request.user.id)
        user = request.user.id
        user_id = user
        message = Message.objects.filter(id=messsage_id)
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)"""

@api_view(["GET"])
@csrf_exempt
@permission_classes([AllowAny])
def get_comments(request, message_id):
    comments = Comment.objects.filter(message=message_id)    #Message.objects.filter(added_by=user)
    serializer = CommentSerializer(comments, many=True)
    return JsonResponse({'comments': serializer.data}, safe=False, status=status.HTTP_200_OK)
# ///   Comentários  /// #


class UpdateMessageView(APIView):
    model = Message
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = MessageSerializer

    def put(self, request, message_id, format=None):
        user = request.user
        message = Message.objects.get(added_by=user, id=message_id)
        serializer = MessageSerializer(message, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["DELETE"])
@csrf_exempt
@permission_classes([IsAuthenticated])
def delete_message(request, message_id):
    user = request.user.id
    try:
        message = Message.objects.get(added_by=user, id=message_id)
        message.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Something went wrong'}, safe=False,
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            )


class CreateInfoView(CreateAPIView):
    model = UserInfo
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = InfoSerializer

    def post(self, request, format=None):
        print(request.user.id)
        user = request.user.id
        user_id = user
        serializer = InfoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
@csrf_exempt
@permission_classes([IsAuthenticated])
def get_info(request):
    user = request.user.id
    info = UserInfo.objects.filter(user_id=user)
    serializer = InfoSerializer(info, many=True)
    return JsonResponse({'info': serializer.data}, safe=False, status=status.HTTP_200_OK)
"""
@api_view(["POST"])
@csrf_exempt
@permission_classes([AllowAny])
def add_message(request):
    payload = json.loads(request.body)
    user = request.user
    try:
        author = Author.objects.get(id=payload["author"])
        message = Message.objects.create(
            title=payload["title"],
            body=payload["body"],
            added_by=user,
            author=author
        )
        serializer = MessageSerializer(message)
        return JsonResponse({'messages': serializer.data}, safe=False, status=status.HTTP_201_CREATED)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Something terrible went wrong'}, safe=False,
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            )
"""
